package com.pillopl.cp

import spock.lang.Specification

class CpApplicationTestGroovy extends Specification {

    def 'sanity check'() {
        given:
            int a = 5
        when:
            int b = a + 33
        then:
            b == 33
    }
}
