package com.pillopl.cp.application;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.unmodifiableList;

@Repository
public class ApplicationRepository {

    private List<Application> offers = new ArrayList<>();

    public boolean noApplications() {
        return offers.isEmpty();
    }

    public void add(Application o) {
        offers.add(o);
    }

    public Optional<Application> getById(Long id) {
        return offers
                .stream()
                .filter(a -> a.getId().equals(id))
                .findFirst();
    }

    public List<Application> getAll() {
        return unmodifiableList(offers);
    }

}
