package com.pillopl.cp.application;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Application {

    private Long id;
    private String clientId;
    private int offerId;

    public Application(long id, String clientId, int offerId) {

        this.id = id;
        this.clientId = clientId;
        this.offerId = offerId;
    }

}
