package com.pillopl.cp.offer;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

@Repository
public class OfferRepository {

    private List<Offer> offers = new ArrayList<>();

    public boolean noOffers() {
        return offers.isEmpty();
    }

    public void add(Offer o) {
        offers.add(o);
    }

    public List<Offer> getAll() {
        return unmodifiableList(offers);
    }


    public Offer findById() {
        return null;
    }
}
